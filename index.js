import { resolve, join } from 'path';

export default function (moduleOptions) {
  const options = {
    ...moduleOptions,
    ...this.options.toast,
  };

  if (!options.namespace) {
    options.namespace = 'toast';
  }

  const { namespace } = options;

  this.addPlugin({
    src: resolve(__dirname, 'plugin.js'),
    fileName: join(namespace, 'plugin.js'),
    ssr: false,
    options,
  });

  ['Toast.js', 'storeModule.js'].forEach((fileName) => {
    this.addTemplate({
      src: resolve(__dirname, fileName),
      fileName: join(namespace, fileName),
      ssr: false,
      options,
    });
  });
}

module.exports.meta = require('./package.json');
