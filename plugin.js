import Toast from './Toast';
import storeModule from './storeModule';

// get the options out using lodash templates
const options = JSON.parse(`<%= JSON.stringify(options) %>`);
// extract the namespace var
const { namespace } = options;

export default function ({ store }, inject) {
  store.registerModule(namespace, storeModule(options), {
    // if the store module already exists, preserve it
    preserveState: Boolean(store.state[namespace]),
  });

  const toast = new Toast(store);

  inject('toast', toast);
}
