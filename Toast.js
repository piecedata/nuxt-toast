export default class Toast {
  constructor(store) {
    this.store = store;
  }

  open(message, options = {}) {
    options.message = message;
    this.store.dispatch('toast/open', options);
  }

  close(id) {
    this.store.dispatch('toast/close', id);
  }

  closeAll() {
    this.store.dispatch('toast/closeAll');
  }

  closeActive() {
    this.store.dispatch('toast/closeActive');
  }

  error(message, options = {}) {
    options.type = 'error';
    this.open(message, options);
  }

  success(message, options = {}) {
    options.type = 'success';
    this.open(message, options);
  }

  info(message, options = {}) {
    options.type = 'info';
    this.open(message, options);
  }

  laravel(errors = {}, options = {}) {
    // get errors fields
    const errorKeys = Object.keys(errors);

    // run only if there are any fields
    if (!errorKeys.length) {
      return;
    }

    const errorKey = errorKeys[0];

    // sometimes backend does not return field errors as array but as string or object
    const messages = Array.isArray(errors[errorKey])
      ? errors[errorKey]
      : (Object.prototype.toString.call(errors[errorKey]) === '[object Object]'
        ? Object.values(errors[errorKey])
        : [errors[errorKey]]);

    if (!messages || !messages.length) {
      return;
    }

    if (!options.type) {
      options.type = 'error';
    }

    this.open(messages[0], options);
  }

  catchLaravel(e, options) {
    const { response } = e;

    if (!response || !response.data || !response.data.errors) {
      if (response && response.data && response.data.error) {
        this.error(response.data.error);
      } else if (e.message) {
        this.error(e.message);
      }

      return;
    }

    const { data: { errors } } = response;

    this.laravel(errors, options);
  }
}
